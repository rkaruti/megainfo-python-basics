from time import sleep, perf_counter
from threading import Thread

def task1():
    print('Starting a task1...')
    sleep(5)
    print('Task 1 done')

def task2():
    print('Starting a task2...')
    sleep(2)
    print('Task 2 done')
start_time = perf_counter()
# create two new threads
t1 = Thread(target=task1)
t2 = Thread(target=task2)
# start the threads
t1.start()
t2.start()
# wait for the threads to complete
t1.join()
t2.join()
end_time = perf_counter()
print(f'It took {end_time- start_time: 0.2f} second(s) to complete.')
