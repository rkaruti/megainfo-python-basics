# Linear Search in Python

# Linear search is a sequential searching algorithm where we start from one end and
# check every element of the list until the desired element is found.
# It is the simplest searching algorithm.

def linearSearch(array, n, x):
    # Going through array sequentially
    for i in range(0, n):
        if (array[i] == x):
            return i
    return -1


array = [2, 4, 0, 1, 9]
print("Elements in the array is ")
print(array)

x = int(input("Enter the element to Search : "))
n = len(array)
result = linearSearch(array, n, x)
if result == -1:
    print("Element not found ", result)
else:
    print("Element found at index: ", result)
