# Example: Bubble Sort is the simplest sorting algorithm that
# works by repeatedly swapping the adjacent elements if they are not in order.
# First Pass 5 1
# 4
# 2
# 8
# Compare the first two elements, and swaps since 5 > 1 1 5 4
# 2
# 8
# Compare 5 and 4 and swap since 5 > 4 1 4 5 2
# 8
# Compare 5 and 2 and swap since 5 > 2 1 4 2 5 8
# Compare 5 and 8 and since 5 < 8, no swap.
#
# Second Pass 1 4 2 5 8
# Compare the first two elements, and as 1 < 4, no swap. 1 4 2 5 8
# Compare 4 and 2, swap since 4 > 2 1 2 4 5 8
# Compare 4 and 5, no swap. 1 2 4 5 8
# Compare 5 and 8 and no swap.
#
# Third Pass 1 2 4 5 8
# Compare the first two elements and no swap. 1 2 4 5 8
# Compare 2 and 4, no swap. 1 2 4 5 8
# Compare 4 and 5, no swap. 1 2 4 5 8
# Compare 5 and 8, no swap.
#
# Fourth Pass 1 2 4 5 8
# Compare the first two elements and no swap. 1 2 4 5 8
# Compare 2 and 4, no swap. 1 2 4 5 8
# Compare 4 and 5, no swap. 1 2 4 5 8
# Compare 5 and 8, no swap.

# Program for implementing Bubble Sort
def b_sort(a):
    n = len(a)
    for i in range(n):
        for j in range(0, n - i - 1):
            if a[j] > a[j + 1]:
                a[j], a[j + 1] = a[j + 1], a[j]


print("Enter elements into list: ")
a = [int(x) for x in input().split()]
b_sort(a)
print("The sorted list is ", a)
