# Program for implementing Insertion Sort
# Insertion sort is a sorting algorithm that places an unsorted element
# at its suitable place in each iteration
def i_sort(a):
    for i in range(1, len(a)):
        temp = a[i]
        pos = i
        while pos > 0 and a[pos - 1] > temp:
            a[pos] = a[pos - 1]
            pos -= 1
            a[pos] = temp


print("Enter elements into list: ")
a = [int(x) for x in input().split()]
i_sort(a)
print("The sorted list is ", a)
