# Binary Search in Python

# Binary search is a searching algorithm which is used to search an element from a
# sorted array. It cannot be used to search from an unsorted array.
# Binary search is an efficient algorithm and is better than linear search in terms
# of time complexity.

# The time complexity of linear search is O(n). Whereas the time complexity of
# binary search is O(log n).
# Hence, binary search is efficient and faster-searching algorithm but can be
# used only for searching
# from a sorted array.

def binarySearch(array, x, low, high):
    # Repeat until the pointers low and high meet each other
    print("low=" + str(low))
    print("high=" + str(high))
    while low <= high:

        mid = low + (high - low) // 2
        print("mid=" + str(mid))
        print("array(mid)=" + str(array[mid]))
        print("low=" + str(low))
        print("high=" + str(high))
        if array[mid] == x:
            return mid

        elif array[mid] < x:
            low = mid + 1

        else:
            high = mid - 1

    return -1


array = [33, 4, 5, 6, 17, 8, 9]
array.sort()
print("Elements in the array is ")
print(array)
x = int(input("Enter the element to Search "))
result = binarySearch(array, x, 0, len(array) - 1)

if result != -1:
    print("Element is present at Index " + str(result))
else:
    print("Not found")
