# Program for implementation of Quick Sort

# Quicksort is a divide-and-conquer algorithm.
# It works by selecting a 'pivot' element from the array and partitioning the
# other elements into two sub-arrays, according to whether they are less than or
# greater than the pivot.

# Eg: Comparing 44 to the right-side elements, and if right-side elements are
# smaller than 44, then swap it. As 22 is smaller than 44 so swap them.
# 22 33 11 55 77 90 40 60 99 44 88.
# Now comparing 44 to the left side element and the element must be
# greater than 44 then swap them

def q_sort(a, low, high):
    if low < high:
        pivotpos = partition(a, low, high)
        q_sort(a, low, pivotpos-1)
        q_sort(a, pivotpos+1, high)

def partition(a, low, high):
    pivotvalue = a[low]
    up = low+1
    down = high
    done = False
    while not done:
        while up <= down and a[up] <= pivotvalue:
         up+=1
        while down >= up and a[down] >= pivotvalue:
             down-=1
             if down < up:
                   done=True
             else:
                    temp=a[up]
                    a[up]=a[down]
                    a[down]=temp

        temp=a[low]
        a[low]=a[down]
        a[down]=temp
        return down


print("Enter elements into list: ")
a = [int(x) for x in input().split()]
high = len(a)
q_sort(a, 0, high-1)
print("The sorted list is ", a)

