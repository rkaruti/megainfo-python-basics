# Program for implementing Selection Sort

# Selection sort is a sorting algorithm that selects the
# smallest element from an unsorted list in each iteration and
# places that element at the beginning of the unsorted list

def s_sort(a):
    for i in range(len(a)):
        least = i
        for k in range(i + 1, len(a)):
            if a[k] < a[least]:
                least = k
        swap(a, least, i)


def swap(a, least, i):
    temp = a[least]
    a[least] = a[i]
    a[i] = temp


print("Enter elements into list: ")
a = [int(x) for x in input().split()]
s_sort(a)
print("The sorted list is ", a)
