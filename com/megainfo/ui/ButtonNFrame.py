import tkinter as tk

app = tk.Tk()

app.geometry('200x200')
app.resizable(0, 0)

backFrame = tk.Frame(master=app, width=200, height=200, bg='white')
backFrame.pack()
backFrame.propagate(0)

button1 = tk.Button(master=backFrame,text='Button 1', bg='blue', fg='red').pack()
button2 = tk.Button(master=backFrame, text='Button 2', bg='gray', fg='green').pack()
button3 = tk.Label(master=backFrame, text='Button 3', bg='red', fg='blue').pack()

app.mainloop()
