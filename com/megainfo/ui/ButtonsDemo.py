import tkinter as tk


# sf.scr.geometry("1366x768")

def write_slogan():
    print("Tkinter is easy to use!")


root = tk.Tk()
frame = tk.Frame(root, width=100, height=200)
frame.pack(fill=None, expand=False)

button = tk.Button(frame, text="QUIT", fg="red", command=quit)
button.pack(side=tk.LEFT)
slogan = tk.Button(frame, text="Hello", command=write_slogan)
slogan.pack(side=tk.LEFT)

root.mainloop()