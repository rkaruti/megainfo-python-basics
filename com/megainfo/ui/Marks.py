from tkinter import *

top = Tk()

resultStr = StringVar()
resultStr.set("Total ")

resultStr1 = StringVar()
resultStr1.set("Avg ")

resultStr2 = StringVar()
resultStr2.set("Result ")

number1Label = Label(text="Roll no")
number1Label.pack()
number1Box = Entry()
number1Box.pack()

number2Label = Label(text="Kannada")
number2Label.pack()
number2Box = Entry()
number2Box.pack()

number3Label = Label(text="English")
number3Label.pack()
number3Box = Entry()
number3Box.pack()

number4Label = Label(text="Mathematics")
number4Label.pack()
number4Box = Entry()
number4Box.pack()

number5Label = Label(text="Science")
number5Label.pack()
number5Box = Entry()
number5Box.pack()

number6Label = Label(text="Social Science")
number6Label.pack()
number6Box = Entry()
number6Box.pack()

resultLabel = Label(textvariable=resultStr)
resultLabel.pack()

resultLabel1 = Label(textvariable=resultStr1)
resultLabel1.pack()

resultLabel2 = Label(textvariable=resultStr2)
resultLabel2.pack()


def addNo():
    no2 = int(number2Box.get())
    no3 = int(number3Box.get())
    no4 = int(number4Box.get())
    no5 = int(number5Box.get())
    no6 = int(number6Box.get())
    no7 = no2 + no3 + no4 + no5 + no6
    no8 = no7 / 5

    if no8 >= 35:
        result = "pass"

    else:
        result = "fail"

    resultStr.set("Total Marks " + str(no7))
    resultStr1.set("Percentage " + str(no8))
    resultStr2.set("Your Result " + str(result))


but = Button(text="Add", command=addNo)
but.pack()

top.mainloop()
