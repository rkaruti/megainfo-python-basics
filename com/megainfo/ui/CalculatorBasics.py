from tkinter import *

root = Tk()
root.title("Addition of two numbers")
fn = StringVar()
sn = StringVar()
resultStr = StringVar()

number1Label = Label(root, text="First Number:")
number1Label.place(x=20, y=10)
number1Box = Entry(root, textvar=fn)
number1Box.place(x=100, y=10)

number2Label = Label(root, text="Second Number:")
number2Label.place(x=20, y=40)
number2Box = Entry(root, textvar=sn)
number2Box.place(x=100, y=40)

number3Label = Label(root, text="Result")
number3Label.place(x=20, y=80)
number3Box = Entry(root, textvar=resultStr)
number3Box.place(x=100, y=80)


##resultLabel = Label(textvariable=resultStr)
##
##resultLabel.pack()

def addNo():
    no1 = int(number1Box.get())
    no2 = int(number2Box.get())
    no3 = no1 + no2

    resultStr.set(str(no3))


but = Button(text="Add", command=addNo)
but.place(x=100, y=120)

root.mainloop()