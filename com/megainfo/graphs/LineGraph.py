# importing the required module
import matplotlib.pyplot as plt

# x axis values
x = ["Test-1","Test-2","Test-3"]
# corresponding y axis values
y = [68,62,78]

# plotting the points
plt.plot(x, y)

# naming the x-axis
plt.xlabel('x - axis')
# naming the y-axis
plt.ylabel('y - axis')

# giving a title to my graph
plt.title('My first graph!')

# function to show the plot
plt.show()
