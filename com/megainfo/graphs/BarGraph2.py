##Matplotlib is an amazing visualization library in Python for 2D plots of arrays. Matplotlib is a multi-platform data visualization library built on NumPy arrays and designed to work with the broader SciPy stack.
##It was introduced by John Hunter in the year 2002.
##
##One of the greatest benefits of visualization is that it allows us visual access to huge amounts of data in easily digestible visuals.
##Matplotlib consists of several plots like line, bar, scatter, histogram etc.

## python -mpip install -U matplotlib


# importing matplotlib module
##from matplotlib import pyplot as plt

### x-axis values
##x = [5, 2, 9, 4, 7]
##
### Y-axis values
##y = [10, 5, 8, 4, 2]
##
### Function to plot
##plt.plot(x,y)
##
### function to show the plot
##plt.show()


########################################################

### importing matplotlib module
##from matplotlib import pyplot as plt
##
### x-axis values
##x = [5, 2, 9, 4, 7]
##
### Y-axis values
##y = [10, 5, 8, 4, 2]
##
### Function to plot the bar
##plt.bar(x,y)
##
### function to show the plot
##plt.show()

#######################################################

# importing matplotlib module
##from matplotlib import pyplot as plt
##
### x-axis values
##x = [5, 2, 9, 4, 7]
##
### Y-axis values
##y = [10, 5, 8, 4, 2]
##
### Function to plot scatter
##plt.scatter(x, y)
##
### function to show the plot
##plt.show()

########################################################

### Import libraries
##from matplotlib import pyplot as plt
##import numpy as np
##
##
### Creating dataset
##cars = ['AUDI', 'BMW', 'FORD',
##		'TESLA', 'JAGUAR', 'MERCEDES']
##
##data = [23, 17, 35, 29, 12, 41]
##
### Creating plot
##fig = plt.figure(figsize =(10, 7))
##plt.pie(data, labels = cars)
##
### show plot
##plt.show()

##########################################################
# Import libraries
##import numpy as np
##import matplotlib.pyplot as plt
##
##
### Creating dataset
##cars = ['AUDI', 'BMW', 'FORD',
##		'TESLA', 'JAGUAR', 'MERCEDES']
##
##data = [23, 17, 35, 29, 12, 41]
##
##
### Creating explode data
##explode = (0.1, 0.0, 0.2, 0.3, 0.0, 0.0)
##
### Creating color parameters
##colors = ( "orange", "cyan", "brown",
##		"grey", "indigo", "beige")
##
### Wedge properties
##wp = { 'linewidth' : 1, 'edgecolor' : "red" }
##
### Creating autocpt arguments
##def func(pct, allvalues):
##	absolute = int(pct / 100.*np.sum(allvalues))
##	return "{:.1f}%\n({:d} g)".format(pct, absolute)
##
### Creating plot
##fig, ax = plt.subplots(figsize =(10, 7))
##wedges, texts, autotexts = ax.pie(data,
##								autopct = lambda pct: func(pct, data),
##								explode = explode,
##								labels = cars,
##								shadow = True,
##								colors = colors,
##								startangle = 45,
##								wedgeprops = wp,
##								textprops = dict(color ="magenta"))
##
### Adding legend
##ax.legend(wedges, cars,
##		title ="Cars",
##		loc ="center left",
##		bbox_to_anchor =(1, 0, 0.5, 1))
##
##plt.setp(autotexts, size = 8, weight ="bold")
##ax.set_title("Customizing pie chart")
##
### show plot
##plt.show()

#########################################################################################
### Import libraries
##from matplotlib import pyplot as plt
##import numpy as np
##
##
### Creating dataset
##cars = ['AUDI', 'BMW', 'FORD',
##		'TESLA', 'JAGUAR', 'MERCEDES']
##
##data = [23, 17, 35, 29, 12, 41]
##
### Creating plot
##fig = plt.figure(figsize =(10, 7))
##plt.pie(data, labels = cars)
##
### show plot
##plt.show()

#############################################################################

##from matplotlib import pyplot as plt
##
##x = [5,2,7]
##y = [2,16,4]
##plt.plot(x,y)
##plt.title('Info')
##plt.ylabel('Y axis')
##plt.xlabel('X axis')
##plt.show()

###########################################################################

##from matplotlib import pyplot as plt
##from matplotlib import style
##
##style.use('ggplot')
##x = [5,8,10]
##y = [12,16,6]
##x2 = [6,9,11]
##y2 = [6,15,7]
##plt.plot(x,y,'g',label='line one', linewidth=5)
##plt.plot(x2,y2,'c',label='line two',linewidth=5)
##plt.title('Epic Info')
##plt.ylabel('Y axis')
##plt.xlabel('X axis')
##plt.legend()
##plt.grid(True,color='k')
##plt.show()

############################################################3
from matplotlib import pyplot as plt

plt.bar([0.25, 1.25, 2.25, 3.25, 4.25], [50, 40, 70, 80, 20],
        label="BMW", width=.5)
plt.bar([.75, 1.75, 2.75, 3.75, 4.75], [80, 20, 20, 50, 60],
        label="Audi", color='r', width=.5)
plt.legend()
plt.xlabel('Days')
plt.ylabel('Distance (kms)')
plt.title('Information')
plt.show()
