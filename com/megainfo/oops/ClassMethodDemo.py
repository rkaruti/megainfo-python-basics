# Define Class and method

class Dog:

    def __init__(self, name, age):
        self.name = name
        self.age = age
        print("Input Name and Age", name, age)

    # Instance method
    def description(self):
        return f"{self.name} is {self.age} years old"

    # Another instance method
    def speak(self, sound):
        return f"{self.name} says {sound}"


if __name__ == '__main__':
    dog = Dog("Tommy", 8)
