import pandas as pd
import seaborn as sns

df = pd.read_csv('../storage/DATA1.csv')
print(df.head(5))

print(df.isnull().sum())
# this will give the sum of all the null values in the dataset.
df1 = df.dropna(axis=0, how='any')
# this will drop rows with null values.
import matplotlib.pyplot as plt

fig, ax = plt.subplots(figsize=(16, 8))
ax.scatter(df['SALARYF'], df['SALARYT'])
ax.set_xlabel('SALARYF')
ax.set_ylabel('SALARYT')
plt.show()
sns.countplot(x="WTYPE", data=df)
sns.countplot(x="WTYPE", hue="FREQUENCY", data=df)
sns.countplot(hue="WTYPE", x="POSTYPE", data=df)
df["SALARYF"].plot.hist()
df["SALARYT"].plot.hist()