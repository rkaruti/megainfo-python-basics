# Define Python user-defined exceptions
class Error(Exception):
    """Base class for other exceptions"""
    pass


class ValueTooSmallError(Error):
    """Raised when the input value is too small"""
    pass


class ValueTooLargeError(Error):
    """Raised when the input value is too large"""
    pass


# You need to have 18 number to get Register for Voter ID.
number = 18
# User Enter try different number(s) for different result
while True:
    try:
        i_num = int(input("Enter a number: "))
        if i_num < number:
            raise ValueTooSmallError
        elif i_num > number:
            raise ValueTooLargeError
        break
    except ValueTooSmallError:
        print("You are small to Vote, try other Number!")
        print()
    except ValueTooLargeError:
        print("You are Eligible to Vote, try other Number too!")
        print()

print("Congratulations! You are Eligible to get Voter ID.")
