# Exception Demo
x = float(input('Enter a number: '))
# Enter y value to '0' showcase Arithmetic Exception
y = float(input('Enter a number: '))
try:
    z = x/y
except ZeroDivisionError as error:
    print("You are Diving by Zero:", error)
else:
    print("No Exception so executing else block!!!")
    print('Result:', x, 'Divided by', y, 'equals: ', z)
finally:
    print("Always Finally Block Executes!!!")
