# Exception Demo
x = float(input('Enter a number: '))
# Enter y value to '0' showcase Arithmetic Exception
y = float(input('Enter a number: '))
try:
    z = x/y
    print(x, 'Divided by', y, 'equals: ', z)
except ZeroDivisionError as error:
    print("You are Diving by Zero:", error)
except IndexError as error:
    print("Entered index is value not Found:", error)
z = x + y
print("Addition of x and y =", z)
