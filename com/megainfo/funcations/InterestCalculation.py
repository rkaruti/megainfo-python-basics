from AdvancedCalculator import simpleInterest, compoundInterest

global interestType
global interestValue
inputVal = int(input("Enter\n 1 for Simple Interest\n 2 for Compound Interest "))
if inputVal == 1:
    interestType = "Simple "
    p = input("Enter the Principal amount:")
    t = input("Enter the time:")
    r = input("Enter the Rate Of Interest:")
    interestValue = simpleInterest(p, t, r)
elif inputVal == 2:
    interestType = "Compound "
    p = input("Enter starting principle please. ")
    n = input("Enter Compound interest rate.(daily, monthly, quarterly, half-year, yearly) ")
    r = input("Enter annual interest amount. (decimal) ")
    t = input("Enter the amount of years. ")
    interestValue = compoundInterest(p, n, r, t)
print(interestType + "Interest is:", interestValue)
