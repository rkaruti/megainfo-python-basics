def average(input_nos):
    total = 0

    for number in input_nos:
        total = total + number

        avg = total / len(input_nos)
        return avg


def findObesity(height, weight):
    message = ""
    bmi = weight / (height / 100) ** 2
    if bmi <= 18.4:
        message = "You are underweight."
    elif bmi <= 24.9:
        message = "You are healthy."
    elif bmi <= 29.9:
        message = "You are over weight."
    elif bmi <= 34.9:
        message = "You are severely over weight."
    elif bmi <= 39.9:
        message = "You are obese."
    else:
        message = "You are severely obese."
    return message


def simpleInterest(p, t, r):
    p = float(p)
    t = float(t)
    r = float(r)
    si = (p * t * r)/100
    return si


def compoundInterest(p, n, r, t):
    p = int(p)
    n = int(n)
    r = float(r)
    t = int(t)
    ci = p*(1+(r/(100*n))**(n*t))
    return ci
