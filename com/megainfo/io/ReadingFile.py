# Read file content
#file = open("info.txt")
#print("Open Existing File: ", file)

file = open("info.txt", 'r')
print("Open Existing File: ", file)

#info = file.read()
#print("File Content ", info)

# Read number of Chars
#info = file.read(100)
#print("File Content ", info)

#info = file.readline(3)
#print("File Content ", info)

info = file.readlines()
print("File Content ", info)
