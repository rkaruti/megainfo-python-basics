# Python program to count the number of lines in a text file

# Opening a file
file = open("info.txt", "r")
count = 0

# Reading from file
Content = file.read()
CoList = Content.split("\n")

for i in CoList:
    if i:
        count += 1

print("Number of lines in the file are:", count)
