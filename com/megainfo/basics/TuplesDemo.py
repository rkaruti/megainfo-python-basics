# Tuple
tupleExample = ("Apple", "Banana", "Kiwi", "Mango")
print("Tuple Data ", tupleExample)

# Tuple duplicates
tupleExample = ("Apple", "Banana", "Kiwi", "Mango", "Apple")
print("Tuple Data ", tupleExample)

# Tuple length
print("Tuple Length", len(tupleExample))

# Tuple with Mixed data types
tupleExampleMix = ("Apple", 40, "Kiwi", 68, True)
print("Tuple Data ", tupleExampleMix)

# Assign Tuple Item chars
fruitsTuple = ("Apple", "Banana", "Cherry")
(green, yellow, red) = fruitsTuple
print(green)
print(yellow)
print(red)

fruitsTuple = ("Apple", "Banana", "Cherry", "Strawberry")
(green, yellow, *red) = fruitsTuple
print(green)
print(yellow)
print(red)

# Tuple Looping
fruitsTuple = ("Apple", "Banana", "Cherry")
for i in range(len(fruitsTuple)):
    print(fruitsTuple[i])
