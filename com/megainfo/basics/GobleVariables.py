# Addition

def addTwo(x, y):
    addition = x + y
    print("Sum of 2 numbers", addition)


number1 = float(input("Please Enter First number "))
number2 = float(input("Please Enter Second number "))
addTwo(number1, number2)


def addThree(x, y, z):
    addition = x + y + z
    print("Sum of 3 numbers", addition)


number1 = float(input("Please Enter First number "))
number2 = float(input("Please Enter Second number "))
number3 = float(input("Please Enter Third number "))
addThree(number1, number2, number3)
