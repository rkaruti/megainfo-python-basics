# Different variables
first_name = "Rose"
last_name = 'Marry'
gender = 'M'
age = 17
rating = 4.5
eligibleToVote = True

print("Student's First Name" + first_name)
print('Student Last Nae ')
print("Gender ", gender)
print("Age ", str(age))
print("Rating Format 1 : " + f'{rating:.2f}')
print("Rating Format 2 : " + '%.4f' % rating)

if age >= 18:
    eligibleToVote = True
    print("Eligible for Voting: " + str(eligibleToVote))
else:
    eligibleToVote = False
    print("Not Eligible for Voting:", eligibleToVote)

print("My City is ", "Bangalore")
print("Vijayanagar Pin Code is ", 560040)
print("Bangalore is in Karnataka?", True)
city = "Bangalore"
print("My City is ", city)
