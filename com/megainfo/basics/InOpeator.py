name = "Welcome to RNS Collage"
print(name.find('RNS'))

fruits = ['Banana', 'Apple', 'Mango', 'Pineapple', 'Orange']

# in operator
print("Mango" in fruits)
print("Grapes" in fruits)

# in not operator
print("Mango" not in fruits)
print("Grapes" not in fruits)
