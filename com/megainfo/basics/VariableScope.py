# Scope of a variable

x = 200


def localFun():
    x = 10
    print(x)


def localFun2():
    x = 20
    print(x)


x = 100
localFun()
print(x)
localFun2()
print(x)