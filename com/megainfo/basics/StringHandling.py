# String can be defined two ways
greeting = "Welcome to "
name = 'RNS Collage'
print(greeting + name)

# Multi-line String
aboutBangalore = """Bangalore is Capital City of Karnataka, 
Bangalore is also Called as Silicon City,
It has lots of Software Companies."""
print("RNS Collage Address", aboutBangalore)

# String as array
message = "Welcome to Bangalore";
# Print the letter based on given index value
print(message[11])

# String slicing for 'Welcome to Bangalore'
message = "Welcome to Bangalore"
# Print string from starting ending position (11 to 20)
cityName = message[11:20]
print("City Name is :", cityName)
# Print string from starting position
print("Print string from starting to position 7:", message[:7])
# Print string from position to end
print("Print string from position 11 to end:", message[11:])

# Looping though string
for x in cityName:
    print(x)

# Find word in string
word = "Bangalore" in message
print("Bangalore found in string?", word)

