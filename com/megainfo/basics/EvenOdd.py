# For loop and %
for x in range(11):
    if x % 2 == 0:
        print("Even Numbers", x)

# For loop with range
for x in range(0, 11, 2):
    print("Even", x)


# Recursive way
def print_evens(i):
    if i > 10:
        return
    print("Recursive", i)
    print_evens(i + 2)


print_evens(0)
