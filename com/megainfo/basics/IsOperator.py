# Program for is operator usage

x = [1, 2, 3]
y = [3, 2, 1]
print(x == y)  # Prints out True
print(x is y)  # Prints out False

print("Input Values")
print("X ", x)
print("Y ", y)

x = y
print("After Exchange Values")
print("X ", x)
print("Y ", y)
