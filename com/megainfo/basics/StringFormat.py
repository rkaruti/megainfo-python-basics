# String formatting is important thing to know
no = 21
city = "Bangaluru"
state = "Karnataka"
country = "India"

print("My Office Located in", city)
office = "My Office Located in {}"
print(office.format(city))
address = "Office Address: #{}, City :{}, State :{}, Country :{}."
print(address.format(no, city, state, country))
# Other way to use formatter
address = "Office Address: #{0}, City :{1}, State :{2}, Country :{3}."
print(address.format(no, city, state, country))
