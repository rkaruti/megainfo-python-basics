# Addition of two numbers

x = 10
y = 20
result = x + y
print("Sum of two numbers ", result)

x = 10.5
y = 20.1
result = x + y
print("Sum of two decimal numbers ", '%.4f' % result)

x = 10
y = 20.3
result = x + y
print("Sum of two mixed numbers ", '%.2f' % result)
