# Please note that marks between 0 and 100 are valid

score = input("Please Enter your marks: ")
score = int(score)

# If Else
if score >= 35:
    print("You passed exam, Congrats!")
else:
    print("Sorry you have failed Exam")

# If and Else If.
# This program is more efficient and gives right result
if score > 100 or score < 0:
    print("Invalid Marks entered")
elif score >= 35:
    print("You passed exam, Congrats!!!")
else:
    print("Sorry you have failed Exam!!!")
