
fruits = ['Banana', 'Apple', 'Mango', 'Pineapple', 'Orange']
print(fruits)
print("List length", len(fruits))
print("Second Item in the list", fruits[1])
print("Last second Item in the list", fruits[-2])
print("Items between position 2:4", fruits[2:4])
print("Items in the List from:", fruits[:2])
print("Items in the List from:", fruits[2:])

# Condition with List
if "Mango" in fruits:
    print("Yes Mango is in List")
else:
    print("No Mango is not in list")

# Changing List Items
print("Original List", fruits)
fruits[2:3] = ["Lemon", "Grapes"]
print("List after", fruits)
# Insert Item in between
print("Original List", fruits)
fruits.insert(2, "Kiwi")
print("List after", fruits)

# Append Item to List
print("Original List", fruits)
fruits.append("Cherry")
print("List after", fruits)

# Remove Item to List
print("Original List", fruits)
fruits.remove("Kiwi")
print("List after", fruits)

# Reverse Item from List
print("Original List", fruits)
fruits.reverse()
print("List after", fruits)

# Remove Last Item from List
print("Original List", fruits)
fruits.pop()
print("List after", fruits)

# Clear List
print("Original List", fruits)
fruits.clear()
print("List after", fruits)

# Looping List
fruits = ['Banana', 'Apple', 'Mango', 'Pineapple', 'Orange', 'Orange']
print("For Loop")
for i in fruits:
    print(i)

# While Loop
print("While Loop")
i = 0
while i < len(fruits):
    print(fruits[i])
    i = i + 1

# Sort a list
print("Original List", fruits)
fruits.sort()
print("List after", fruits)

print("Original List", fruits)
fruits.sort(reverse=True)
print("List after", fruits)
