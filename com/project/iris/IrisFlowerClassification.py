# summarize the data
from pandas import read_csv
# Load dataset
url = "iris.csv"
names = ['sepal-length', 'sepal-width', 'petal-length', 'petal-width', 'class']
dataset = read_csv(url, names=names)
# shape
print(dataset.shape)
# head
print(dataset.head(20))
# descriptions
print(dataset.describe())
# class distribution
print(dataset.groupby('class').size())

##...
### descriptions
##from pandas import read_csv
### Load dataset
##url = "D:\\joshiprojects\\python_notes_programs\\nishchitapythonprgs\\pythoncls\\ML\\Assignment\\car1.csv"
##names = ['sno', 'n', 's', 'e', 'w','MAKE']
##dataset = read_csv(url, names=names)
##print(dataset.describe())
##print(dataset.groupby('MAKE').size())
##
##
